from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files import File
from .models import Arquivo
from .algoritmos import man_dados
from .algoritmos import discretizacao
from .algoritmos import classificacao
from .algoritmos import rotulacao 
import csv
import pandas as pd

def home(request):
	return render(request, 'core/home.html')

@csrf_exempt
def upload_file(request):
	data = {}

	try:
		file = request.FILES['file']		
	except:
		erro = "Selecione um arquivo no formato .csv."
		data['html_content_erro'] = render_to_string('core/parciais/parcial_erro.html',
			{'erro': erro})
		return JsonResponse(data)

	file_name = str(file).split('.')
	formato = file_name.pop()
	file_name = file_name[0].title() + " Dataset"

	if formato != "csv":
		erro = "O Arquivo deve ser no formato .csv."
		data['html_content_erro'] = render_to_string('core/parciais/parcial_erro.html',
			{'erro': erro})
		return JsonResponse(data)

	file_ = Arquivo(arquivo=file)
	file_.save()

	#Lê a base de dados no formato csv
	bd = man_dados.read_csv(file_.arquivo.url)

	colunas_discretizacao = [] # Colunas para a tabela de discretização
	colunas_cluster = [] # Colunas para escolha do cluster (contém todas)

	colunas_discretizacao, colunas_cluster = man_dados.disc_cluster_att(bd)

	data['file_id'] = file_.id
	data['html_content_algorithm'] = render_to_string('core/parciais/parcial_algoritmo.html',
				{'colunas': [], 'colunas2': colunas_cluster})
	data['html_content_file_name'] = render_to_string('core/parciais/parcial_file_name.html', 
				{'file_name': file_name})
	return JsonResponse(data)

def remove_file(request):
	data = {}

	try:
		file_id = request.GET['file_id']
		arquivo = Arquivo.objects.get(pk=file_id)
		arquivo.delete()
	except:
		pass
	return JsonResponse(data)

def download_file(request):
	id = request.GET['file_id']
	file = Arquivo.objects.get(pk=id)
	file = man_dados.read_csv(file.arquivo_discretizado.url)

	response = HttpResponse(file, content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename=new_db.csv'
	return response

def change_disc_table(request):
	data = {}

	cluster = request.GET['cluster']

	file_id = request.GET['file_id']
	file = Arquivo.objects.get(pk=file_id)
	file_path = file.arquivo.url

	bd = man_dados.read_csv(file.arquivo.url)
	colunas, _ = man_dados.disc_cluster_att(bd)
	limites = man_dados.num_instancias(bd, cluster)

	cont = 0
	for ind, c in enumerate(colunas):
		if c == cluster:
			del colunas[ind]
			break

	data['html_content_tabela'] = render_to_string('core/parciais/parcial_tabela_discretizacao.html',
				{'colunas': zip(colunas, limites)})
	return JsonResponse(data)

def show_config(request, alg):
	data = {}

	if alg == "mlp":
		data['html_content_config'] = render_to_string('core/parciais/parcial_mlp_config.html')
	else:
		data['html_content_config'] = render_to_string('core/parciais/parcial_cart_config.html')
	
	return JsonResponse(data)

def run(request):
	erro = None
	data = {}

	# Salva o caminho do arquivo
	file_id = request.GET['file_id']
	file= Arquivo.objects.get(pk=file_id)
	file_path = file.arquivo.url

	# Salva o atributo cluster da base
	try:
		attr_cluster = request.GET['cluster']
	except:
		erro = "O Atributo Cluster deve ser selecionado."
		data['html_content_erro'] = render_to_string('core/parciais/parcial_erro.html',
				{'erro': erro})
		return JsonResponse(data)

	# Verificando se é necessário discretizar
	try:
		no_discretization = request.GET['discretization']
	except:
		no_discretization = None

	faixas = []

	# Verificando se a lista de discretização está vazia
	ind_cluster = 0


	#------------------------------ DEFINIÇÂO DOS PARÂMETROS ------------------------------------------
	# Caminho da base de dados
	base_original = man_dados.read_csv(file_path) 

	# Salva o nome dos atributtos 
	nomes = man_dados.named(base_original)

	# Atributo cluster
	attr_name = attr_cluster

	#limite das faixas 
	limite = man_dados.num_instancias(base_original, attr_name)

	if no_discretization == None:
		for ind, att in enumerate(nomes):
			if att == attr_cluster:
				continue
			try:
				faixas.append(request.GET[att])
			except:
				pass


		# Verifica se existem campos de discretização vazios
		if '' in faixas or '0' in faixas:
			erro = "As Faixas de Discretização devem ser preenchidas corretamente."
			data['html_content_erro'] = render_to_string('core/parciais/parcial_erro.html',
				{'erro': erro})
			return JsonResponse(data)

		faixas = [int(v) for v in faixas]

		for ind, valor in enumerate(faixas):
			if valor > limite[ind] or valor < 1:
				erro = "As Faixas de Discretização devem ser positivas e obdecer aos limites definidos."
				data['html_content_erro'] = render_to_string('core/parciais/parcial_erro.html',
						{'erro': erro})
				return JsonResponse(data)

	#Configurações da rede neural
	classificador = request.GET['algorithm']
	num_folds = 5
	porcentagem_treino = 60
	

	#Configurações do discretizador
	discretizador = 'EFD'

	#Configurações do rotulador
	V = 10

	# Processamento da base 
	frames_originais = man_dados.group_separator(base_original, attr_name)

	# Parâmetros extras da mlp
	conf_mlp_list = ["hidden_layer_sizes", "activation", "learning_rate", "learning_rate_init", "max_iter", "momentum"]
	conf_mlp = {"hidden_layer_sizes": 10, "max_iter": 200}

	if classificador == "MLP":
		try:
			for p in conf_mlp_list:
				v = request.GET[p]
				if p in ["hidden_layer_sizes", "max_iter", ]:
					v = int(v)
				elif p in ["learning_rate_init", "momentum"]:
					v = float(v)
				conf_mlp[p] = v
		except:
			pass

	#---------------------- ESCOLHER UMA DAS ROTINAS -------------------------------------------------------------------------------------------
	if no_discretization is None:
		#Rotina para discretização
		disc = discretizacao.discretizador(base_original, faixas, discretizador, attr_name)
		base_discretizada = disc.ddb
		infor = disc.infor
		frames_discretizados = man_dados.group_separator(base_discretizada, attr_name)
		resultado_classificacao = classificacao.classifica_bd(frames_discretizados, classificador, attr_name, porcentagem_treino, num_folds, **conf_mlp )
		rotulo = rotulacao.rotular(frames_originais, frames_discretizados, resultado_classificacao, V, infor)
		discretizada = True
	else:
		#Rotina sem discretização
		resultado_classificacao = classificacao.classifica_bd(frames_originais, classificador, attr_name, porcentagem_treino, num_folds, **conf_mlp )
		rotulo = rotulacao.rotular(frames_originais, None, resultado_classificacao, V, None)
		discretizada = False

	# Preparação da string do rótulo para a tabela do rótulo.
	lista = []
	for cluster in rotulo:
		cluster2 = []
		cluster = cluster.split('\n')
		cluster.pop() # remove o 'lixo' deixado pelo ultimo \n na linha

		for linha in cluster:
			cluster2.append(linha.split(','))
		lista.append(cluster2)

	data['html_content_rotulo'] = render_to_string('core/parciais/parcial_rotulo.html', 
				{'lista': lista, "discretizada": discretizada})
	return JsonResponse(data)
	
