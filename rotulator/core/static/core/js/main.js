function UploadFile(){
	removeFile();
	$("#erro").html("");
	var form = document.getElementById('form_file');
	var formData = new FormData(form);

	$.ajax({
		type:"POST",
		url: "http://127.0.0.1:8000/upload/",
		data: formData,
			contentType: false,
			processData: false,
		success: function(data){
			if (data.html_content_erro) {
					$("#erro").html(data.html_content_erro);
					$("#erro").show();
			}
			else{
				$("#algorithm").html(data.html_content_algorithm);
				$("#file_name").html(data.html_content_file_name);
				$("#file_select").hide();
				$("#cart_set").hide();
				$("#myProgress").hide();
				document.getElementById("file_id").value = data.file_id;
				document.getElementById("file_id_algorithm").value = data.file_id;
			}
		}
	});
}

function removeFile(){
	var file_id = document.getElementById("file_id").value;

	$.ajax({
		type:"GET",
		url: "http://127.0.0.1:8000/remove/",
		data: {"file_id": file_id},
		dataType: "json",
	});
}
function changeDiscretizationTable(){
	var cluster = document.getElementById("cluster_select").value;
	var file_id = document.getElementById("file_id").value;

	data = {'cluster': cluster, 'file_id': file_id};

	$.ajax({
		type:"GET",
		url: "http://127.0.0.1:8000/change_disc_table/",
		data: data,
		dataType: "json",
		success: function(data){
			$("#tabela_discretizacao").html(data.html_content_tabela);
			changeDiscretization();
		}
	})
}

function run(){
	$("#erro").html("");
	$("#rotulo").html("");
	$("#myProgress").show();

	var formData = $("#form_algorithm").serialize();
	var elem = document.getElementById("myBar");   
  			
  	elem.style.width = 10 + '%'; 
  	elem.innerHTML = 10  + '%';

	$.ajax({
		type: "GET",
			url: "http://127.0.0.1:8000/run/",
			data: formData,
			success: function (data) {
				if (data.html_content_erro) {
					$("#myProgress").hide();
					$("#erro").html(data.html_content_erro);
					$("#erro").show();
					window.scrollTo(0, 0);
				}
				else{
					move();
					setTimeout(function afterOneSecond() {
						$("#rotulo").html(data.html_content_rotulo);
					}, 1000)
				}
			}
	});
}

function changeDiscretization() {
	var checkBox = document.getElementById("discretization");
	if (checkBox.checked == true){
  		$("#colunas").hide();
	} else {
 			$("#colunas").show();
	}
}

function verifyConfigIsVisible(alg){
	if (alg==1 && $("#config").is(":visible")) {
		$("#config").hide();
	}
	else if (alg==1 && $("#config").is(":hidden")) {
		$("#config").show();
	}
	else if (alg==2 && $("#config").is(":visible")) {
		$("#config").hide();
	}
	else{
		$("#config").show();
	}
}

function changeSettings(){
	var mlp = document.getElementById("mlp");
	var cart = document.getElementById("cart");

	if (mlp.checked == true) {
		$("#mlp_set").show();
		$("#cart_set").hide();
		verifyConfigIsVisible(1);
	}
	else if (cart.checked == true) {
		$("#cart_set").show();
		$("#mlp_set").hide();
		verifyConfigIsVisible(2);
	}
}

function hideError(){
	$("#erro").hide();
}

function showConfig(alg){
	verifyConfigIsVisible(alg);

	if(alg == 1){
		$.ajax({
			type: "GET",
			url: "http://127.0.0.1:8000/show_config/mlp",
			data: {},
			success: function (data) {
				$("#config").html(data.html_content_config);
			}
		});
	}
	else{
		$.ajax({
			type: "GET",
			url: "http://127.0.0.1:8000/show_config/cart",
			data: {},
			success: function (data) {
				$("#config").html(data.html_content_config);
			}
		});
	}
	
}

function move() {
	var elem = document.getElementById("myBar");   
	var width = 10;
	var id = setInterval(frame, 10);
	function frame() {
		if (width >= 100) {
				clearInterval(id);
		} else {
			width++; 
			elem.style.width = width + '%'; 
			elem.innerHTML = width * 1  + '%';
		}
	}
}

function downloadCsvFile(){
	var file_id = document.getElementById("file_id").value;
	var data = {'file_id': file_id}

	$.ajax({
		type: "GET",
			url: "http://127.0.0.1:8000/download_file/",
			data: data,
	});
}

window.onbeforeunload = function() {
		removeFile();
}
