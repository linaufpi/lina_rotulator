from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.home, name='home'),
    path('upload/', views.upload_file, name='upload_file'),
    path('remove/', views.remove_file, name='remove_file'),
    path('download_file/', views.download_file, name='download_file'),
    path('change_disc_table/', views.change_disc_table, name='change_disc_table'),
    path('run/', views.run, name='run'),
    path('show_config/<str:alg>', views.show_config, name='show_config'),
]
