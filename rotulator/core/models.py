from django.db import models

class Arquivo(models.Model):
	arquivo = models.FileField(upload_to="uploads/")
	arquivo_discretizado = models.FileField(upload_to="uploads/", null=True)
	arquivo_rotulo = models.FileField(upload_to="uploads", null=True)