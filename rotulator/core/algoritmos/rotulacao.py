from collections import Counter

class rotulador(object):
	def __init__ (self, num, cluster, holdout_val,V):
		self.V = V
		self.medias = [(i, acuracia*100) for i, acuracia in holdout_val]
		self.medias.sort(key=lambda x: x[1], reverse=True)
		self.min = self.medias[0][1]-V
		self.titulos = cluster.columns.values.tolist()
		self.data = cluster.values
		self.num = num+1

	def rotular_bd_naoDiscretizada(self):		
		rotulo = str(len(self.data)) + ","
		for i in range(0, self.data.shape[1]): 
			if self.medias[i][1] >= self.min: 
				most_comun_value = Counter(self.data[:,i]).most_common(1) 
				num_erros, taxa_acerto = self.erro(most_comun_value[0][0], self.data[:,i])
				rotulo +=  self.titulos[i] +"," + str (most_comun_value[0][0]) + "," + str(self.medias[i][1]) +  "%" + "," + str(num_erros) +  "," + str(taxa_acerto) + "%\n"
		return rotulo

	def rotular_bd_discretizada(self, infor, cluster_disc):
		rotulo = str(len(self.data)) + ","
		for i in range(0, self.data.shape[1]):
			if self.medias[i][1] >= self.min: 
				most_comun_values = Counter(cluster_disc.values[:,i]).most_common(1) 
				most_comun_values = int(most_comun_values[0][0]) 
				num_erros, taxa_acerto = self.erro_disc(infor[i][most_comun_values], infor[i][most_comun_values+1], self.data[:,i])
				rotulo += self.titulos[i] + "," + str(round(infor[i][most_comun_values],2)) + " ~ " + str(round(infor[i][most_comun_values + 1],2)) + "," + str(round(self.medias[i][1],2)) + "%" + "," + str(round(num_erros,2)) +  "," + str(round(taxa_acerto,2)) + "%\n"
		return rotulo

	def erro_disc(self, infe_lim, super_lim, list_val):
		num_erros = 0
		for i in list_val:
			if i>=infe_lim and i<=super_lim:
				pass
			else:
				num_erros += 1
		num_acertos = len(list_val)-num_erros
		taxa_acerto = 100*num_acertos/len(list_val)
		return num_erros, taxa_acerto
	
	def erro(self, val, list_val ):
		num_erros = 0
		for i in list_val:
			if i != val:
				num_erros += 1
		num_acertos = len(list_val)-num_erros
		taxa_acerto = 100 *num_acertos/len(list_val)
		return num_erros, taxa_acerto

def rotular( grupos, grupos_disc, classificacao_infor, V, discretizacao_infor):
	rotulo = []
	if discretizacao_infor:		
		for i in range(0, len(grupos)):
			rotulador_ = rotulador(i, grupos[i], classificacao_infor[i], V)
			rotulo.append(rotulador_.rotular_bd_discretizada(discretizacao_infor, grupos_disc[i]))
	else:
		for i in range(0,len(grupos)):
			rotulador_= rotulador(i, grupos[i], classificacao_infor[i], V)
			rotulo.append(rotulador_.rotular_bd_naoDiscretizada())
	return rotulo
