import pandas as pd 
import numpy as np

def read_csv(path):
	bd = pd.read_csv(path,sep=',',parse_dates=True)
	return bd

def named(data):
	attr_names = data.columns.values.tolist()
	return attr_names;

# Recebe a lista dos nomes dos atributos e a lista dos valores da primeira linha da base de dados.
# Retorna uma tupla contendo uma lista com somente os atributos que poderão ser discretizados
# e uma lista contento somente os atributos que podem ser clusters
def disc_cluster_att(data):
	all_columns = named(data) # Todas os nomes dos atributos
	att_disc = all_columns.copy() # Atributos que podem ser discretizados
	att_cluster = [] # Atributos clusters

	columns_types = str(data.dtypes)
	columns_types = columns_types.split("\n")
	columns_types = [ x.split(" ").pop() for x in columns_types]
	columns_types.pop()

	cont = 0
	for c in columns_types:
		if "float" in c:
			pass
		elif "int" in c:
			att_cluster.append(att_disc[cont])
		elif "object" in c:
			att_cluster.append(att_disc[cont])
			del att_disc[cont]
			cont = cont - 1
		cont = cont + 1
		
	return(att_disc, att_cluster)

def group_separator(data, attr_name):
	grouped = data.groupby(attr_name)                
	frames = [] 								           
	for nome, grupo in grouped:
		cluster = grupo
		cluster = cluster.drop([attr_name],axis = 1)
		frames.append(cluster)		
	return frames				

def num_instancias(data, attr_cluster):
	data = data.drop([attr_cluster], axis=1)
	num_instancias = []
	for i in range(0, data.shape[1]):
		values = data.loc[:,data.columns[i]].get_values()
		num_instancias.append(len(sorted(set(values))))
	return num_instancias